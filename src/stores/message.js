function createMessage() {
  let forceUpdate = (timeout, type, text) => {};

  return {
    set: (fu) => forceUpdate = fu,
    success: (text) => forceUpdate(2000, "success", text),
    info: (text) => forceUpdate(3000, "info", text),
    warning: (text) => forceUpdate(3500, "warning", text),
    error: (text) => forceUpdate(10000, "error", text)
  }
}

export const message = createMessage();
