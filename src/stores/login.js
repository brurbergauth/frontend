import { writable } from 'svelte/store';

const lang = "en_US";

function createUserstate() {
  const { subscribe, set, update } = writable(null);

  async function get(addr) {
    await fetch(addr)
      .then(r => r.json())
      .then(data => {
        set(data);
      });
  }

  return {
    subscribe,
    set,
    get
  }
}

export const continents = createTest();

export const systemTexts = createTest();

window.onload = (event) => {
  continents.get(`https://jsonplaceholder.typicode.com/posts`)
};
